### What is this repository for? ###

Big World Setup (BWS): Mod Manager for Baldur's Gate / Enhanced Edition (Trilogy) by dabus

Discussion SHS Forum: http://www.shsforums.net/topic/56670-big-world-setup-an-attempt-to-update-the-program/

Discussion Beamdog  : https://forums.beamdog.com/post/editdiscussion/44476

Changelog: https://bitbucket.org/BigWorldSetup/bigworldsetup/commits/all

Mod Request Template: http://www.shsforums.net/topic/58006-big-world-setup-mod-request-template/

Features:

- downloading mods (please see remarks!)
- easy mod installation for BGT and EET
- correct install order of mods/components (BiG World Project)
- handle mod and components conflicts
- easy backup creation/restoring
- ability to add you own mods

Supported games:

- Baldur's Gate: Enhanced Edition (standalone game)
- Baldur's Gate II: Enhanced Edition (standalone game)
- Enhanced Edition Trilogy ( BG1:EE + SoD + BG2:EE + IWD1:EE + partial IWD2-in-EET ) (not yet enabled!)
- Baldur's Gate 2 (standalone game) / Baldur’s Gate Trilogy ( BG1 + BG2 )
- Icewind Dale / Icewind Dale: Enhanced Edition
- Icewind Dale II / Icewind Dale II: Enhanced Edition
- Planescape: Torment
- Classic Adventures

Supported mods:

- almost all of them

Installation:

0. Quit all games and game editors
1. Disable you antivirus
2. Disable User Account Control
3. Download Big World Setup zip archive and extract it anywhere you want:
4. Execute "BiG World Setup.vbs"

BTW: We need new logo which will cover all game logos :)

### Contribution guidelines ###

* Don't use BitBucket web-interface edit because of the bug 

### How do I contribute? ###

* learn git basics
* fork this repository using "SmartGit" or other prefered tool
* add mods/make other changes
* create pull request for this repo

### Contributors ###
[agb1](http://www.shsforums.net/user/41035-agb1/)

[AL|EN](http://www.shsforums.net/user/10953-alien/)

[Quiet](http://www.shsforums.net/user/13265-quiet/)